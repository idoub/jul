describe('Test Ajax', function () {
  describe('Test basic verbs with callback', function () {
    function check(done, f) {
      try {
        f();
        done();
      } catch (e) {
        done(e);
      }
    }
    it('GET basic object', function (done) {
      jul.ajax({ url: 'https://jsonplaceholder.typicode.com/posts/1' }, function (err, resp) {
        check(done, function () {
          chai.assert.strictEqual(resp.userId, 1);
          chai.assert.strictEqual(resp.title, 'sunt aut facere repellat provident occaecati excepturi optio reprehenderit');
          chai.assert.notStrictEqual(resp.userId, '1');
        })
      })
    });
    it('Handle params object', function (done) {
      jul.ajax({ url: 'https://jsonplaceholder.typicode.com/comments', params: { postId: 1 } }, function (err, resp) {
        check(done, function () {
          chai.assert.isArray(resp);
          chai.assert.strictEqual(resp[3].body, 'non et atque\noccaecati deserunt quas accusantium unde odit nobis qui voluptatem\nquia voluptas consequuntur itaque dolor\net qui rerum deleniti ut occaecati');
        })
      })
    });
    it('Handle array params object', function (done) {
      jul.ajax({ url: 'https://jsonplaceholder.typicode.com/comments', params: { postId: [1, 2, 3] } }, function (err, resp) {
        check(done, function () {
          chai.assert.isArray(resp);
          chai.assert.strictEqual(resp[3].postId, 1);
          chai.assert.strictEqual(resp[3].body, 'non et atque\noccaecati deserunt quas accusantium unde odit nobis qui voluptatem\nquia voluptas consequuntur itaque dolor\net qui rerum deleniti ut occaecati');
          chai.assert.strictEqual(resp[7].postId, 2);
          chai.assert.strictEqual(resp[7].body, 'ut voluptatem corrupti velit\nad voluptatem maiores\net nisi velit vero accusamus maiores\nvoluptates quia aliquid ullam eaque');
          chai.assert.strictEqual(resp[12].postId, 3);
          chai.assert.strictEqual(resp[12].body, 'fuga eos qui dolor rerum\ninventore corporis exercitationem\ncorporis cupiditate et deserunt recusandae est sed quis culpa\neum maiores corporis et');
        })
      })
    });
    it('Handle params object with existing params.', function (done) {
      jul.ajax({ url: 'https://jsonplaceholder.typicode.com/posts?userId=1', params: { userId: 2 } }, function (err, resp) {
        check(done, function () {
          chai.assert.isArray(resp);
          chai.assert.strictEqual(resp[7].userId, 1);
          chai.assert.strictEqual(resp[7].body, 'dignissimos aperiam dolorem qui eum\nfacilis quibusdam animi sint suscipit qui sint possimus cum\nquaerat magni maiores excepturi\nipsam ut commodi dolor voluptatum modi aut vitae');
          chai.assert.strictEqual(resp[18].userId, 2);
          chai.assert.strictEqual(resp[18].body, 'illum quis cupiditate provident sit magnam\nea sed aut omnis\nveniam maiores ullam consequatur atque\nadipisci quo iste expedita sit quos voluptas');
        })
      })
    });
    it('Handle params object with existing array params', function (done) {
      jul.ajax({ url: 'https://jsonplaceholder.typicode.com/posts?userId=1&userId=2', params: { userId: 3 } }, function (err, resp) {
        check(done, function () {
          chai.assert.isArray(resp);
          chai.assert.strictEqual(resp[7].userId, 1);
          chai.assert.strictEqual(resp[7].body, 'dignissimos aperiam dolorem qui eum\nfacilis quibusdam animi sint suscipit qui sint possimus cum\nquaerat magni maiores excepturi\nipsam ut commodi dolor voluptatum modi aut vitae');
          chai.assert.strictEqual(resp[18].userId, 2);
          chai.assert.strictEqual(resp[18].body, 'illum quis cupiditate provident sit magnam\nea sed aut omnis\nveniam maiores ullam consequatur atque\nadipisci quo iste expedita sit quos voluptas');
          chai.assert.strictEqual(resp[20].userId, 3);
          chai.assert.strictEqual(resp[20].body, 'repellat aliquid praesentium dolorem quo\nsed totam minus non itaque\nnihil labore molestiae sunt dolor eveniet hic recusandae veniam\ntempora et tenetur expedita sunt');
        })
      })
    });
  });
});
