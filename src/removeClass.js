import jul from './jul.js';
import './each.js';
import './is.js';

/**
 * Removes a class or classes from wrapped elements
 *
 * @param  {string|string[]} cls - A class or array of classes that you want to remove from the wrapped elements.
 * @return {jul.prototype}       - The jul that the method was called on.
 */
jul.prototype.removeClass = function (cls) {
  if (!jul.is('array', cls)) cls = [cls];
  this.each(function (el) {
    var classList = el.className.split(' ');
    for (var j = 0; j < cls.length; j++) {
      var index = classList.indexOf(cls[j]);
      if (index !== -1) classList.splice(index, 1);
    }
    el.className = classList.join(' ');
  });
  return this;
};
