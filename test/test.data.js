describe('Test Data', function () {
  it('Grab single data attribute from single element', function () {
    chai.assert.deepEqual(jul('#div2').data(), { attr1: 'same name, diff el' })
  });
  it('Grab multiple data attributes from single element', function () {
    chai.assert.deepEqual(jul('#div1').data(), { attr1: 'attribute 1', attr2: 'Another Attribute' })
  });
  it('Grab multiple data attributes from multiple elements', function () {
    chai.assert.deepEqual(jul('.red').data(), { attr1: ['attribute 1', 'same name, diff el'], attr2: 'Another Attribute' })
  });
});
