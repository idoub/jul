describe('Test Extend', function () {
  describe('Test jul().extend', function () {
    var obj1, obj2;
    beforeEach(function () {
      obj1 = {
        prop1: true,
        prop2: 'foo',
        prop3: 12345
      }
      obj2 = {
        prop4: false,
        prop5: 'bar',
        prop6: 54321
      }
    });
    it('Extend works', function () {
      jul(obj1).extend(obj2);
      chai.assert.containsAllKeys(obj1, obj2);
    });
    it('Deep Extend', function () {
      obj2.deep = jul.extend({}, obj2);
      jul(obj1).extend(obj2);
      chai.assert.containsAllDeepKeys(obj1, obj2);
    })
  });

  describe('Test jul.extend', function () {
    var obj1, obj2;
    beforeEach(function () {
      obj1 = {
        prop1: true,
        prop2: 'foo',
        prop3: 12345
      }
      obj2 = {
        prop4: false,
        prop5: 'bar',
        prop6: 54321
      }
    });
    it('Extend works', function () {
      jul.extend(obj1, obj2);
      chai.assert.containsAllKeys(obj1, obj2);
    });
    it('Deep Extend', function () {
      obj2.deep = { key1: 'value1', key2: 'value2' }
      jul.extend(obj1, obj2);
      chai.assert.containsAllDeepKeys(obj1, obj2);
    });
    it('Can\'t extend a non-object', function () {
      var str = 'foo';
      jul.extend(str, obj1);
      chai.assert.isString('foo');
    });
    it('Can\'t extend an object with non-objects', function () {
      var tmp = jul.extend({}, obj1);
      jul.extend(obj1, 'foo');
      chai.assert.deepEqual(tmp, obj1);
    });
    it('Extends several objects', function () {
      var obj3 = { prop7: { prop8: { prop9: "Inception!" } } }
      var tmp = {};
      jul.extend(tmp, obj1, obj2, obj3);
      chai.assert.containsAllDeepKeys(tmp, obj1);
      chai.assert.containsAllDeepKeys(tmp, obj2);
      chai.assert.containsAllDeepKeys(tmp, obj3);
    });
    it('Extends deep objects', function () {
      obj1.prop7 = obj2;
      var obj3 = { prop7: { prop8: { prop9: "Inception!" } } }
      jul.extend(obj1, obj3);
      chai.assert.containsAllDeepKeys(obj1, obj3);
    });
  });
});
