import jul from './jul.js';
import './each.js';

/**
 * Insert content after the currently wrapped elements
 *
 * @param  {node|string}   content - The content you want inserted after each of the currently wrapped elements.
 * @return {jul.prototype}         - The jul that the method was called on.
 */
jul.prototype.after = function (content) {
  if (content instanceof Node) content = content.outerHTML;
  return this.each(function (e) {
    e.insertAdjacentHTML('afterend', content);
  });
};
