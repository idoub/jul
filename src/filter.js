import jul from './jul.js';

/**
 * Loop through every member of an arraylike object wrapped by an jul and return an array that matches the filter callback.
 *
 *
 * @param  {function} fn - The filter function. If the function returns true , then the value of the current arraylike object will be added to the new array.
 * @param  {...*} params - Any params you want passed to the filter function.
 * @return {array}       - The new array with only the values of the current wrapped arraylike object that met the filter criteria.
 *
 * @example
 *  jul(['jewel','jade','ruby','jasper','diamond','turquoise']).filter(
 *      function(element,index,array,character){
 *          return element.indexOf(character) > -1;
 *      },
 *      'j'
 *  )
 */
jul.prototype.filter = function (fn, params) {
  var args = [].slice.call(arguments), me = this;
  args.shift();
  var out = [];
  for (var i = me.e.length; i--;) {
    if (fn.apply(me, [me.e[i], i, me.e].concat(args)) === true)
      out.unshift(me.e[i]);
  }
  return out;
};
