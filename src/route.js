import jul from './jul.js';
import './ready.js';
import './bind.js';

/**
 * @typedef  route
 * @property {string}   name    - Give this route a name you can use as a reference.
 * @property {string}   path    - Define the path for this route.
 * @property {function} handler - The function you want to run when this route is hit.
 */

/**
 * A tiny, fairly flexible router.
 *
 * @class
 */
jul.route = (function () {
  var routes = {};

  var runRoutes = function (path, push) {
    var foundRoute = false;
    for (var key in routes) {
      var routed = routes[key].path.exec(path);
      if (routed) {
        if (push) history.pushState(jul.readModel(), routes[key].name, path);
        routes[key].handler(routed.slice(1))
        foundRoute = true;
      }
    }
    captureLinks();
    return foundRoute;
  };

  var captureLinks = function () {
    var ls = document.links
    for (var i = 0; i < ls.length; ++i) {
      if (ls[i].hasAttribute('data-route-collected')) continue;
      ls[i].addEventListener('click', function (evt) {
        if (runRoutes(evt.target.href.replace(evt.target.origin, ''), true))
          evt.preventDefault();
      });
      ls[i].setAttribute('data-route-collected', 'true');
    }
  }

  jul.ready(function () {
    captureLinks();

    window.addEventListener('popstate', function (evt) {
      runRoutes(document.location.pathname);
      if (history.state) jul.writeModel(history.state);
    });

    runRoutes(document.location.pathname);
  });

  /**
   * @constructs jul.route
   * @param {route} route - The route definition you want to declare.
   */
  return function (route) {
    if (route instanceof Array) {
      route.forEach(function (def) {
        routes[def.name] = def;
      })
    } else {
      routes[route.name] = route;
    }
  };
}());
