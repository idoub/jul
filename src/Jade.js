/**
 * Jade: A type of JUL!
 *
 * A Jade is an observable object and is used to bind objects to elements on the
 * page. It has several properties that help it to maintain a tree structure,
 * convert to and from a normal object, and notify listeners when the object
 * changes.
 *
 * @constructs Jade
 * @param {object} obj - A prior object that you want to turn into a Jade
 */
function Jade(obj) {
  var parent,
    listeners = [],
    value = '',
    me = this;

  var isPropWritable = function (prop) {
    return prop && typeof prop === 'object' && !Array.isArray(prop);
  };

  /**
   * The value of this Jade. Starts as a string but can be assigned anything.
   *
   * @memberof Jade
   * @name Jade#value
   * @property {object} value - The value contained by this Jade
   */
  Object.defineProperty(me, 'value', {
    get: function () {
      return value;
    },
    set: function (newValue) {
      value = newValue;
      this.notify();
      if (parent) parent.notify();
    }
  });

  /**
   * A method provided to add a property to the current Jade. The property that
   * is added will itself be a Jade and will be provided with a reference to the
   * current Jade as it's parent.
   *
   * @memberof Jade
   * @method
   * @name Jade#addProp
   * @param {string} name - The name of the new property you want to create.
   */
  Object.defineProperty(me, 'addProp', {
    value: function (name) {
      this[name] = new Jade();
      this[name].setParent(this);
    }
  });

  /**
   * This sets a reference in the current Jade to any object you want to be
   * defined as the Jade's parent. This helps with heirarchical referencing.
   *
   * @memberof Jade
   * @method
   * @name Jade#setParent
   * @param {object} newParent - A reference to any object that you want to
   * define as this Jade's parent.
   */
  Object.defineProperty(me, 'setParent', {
    value: function (newParent) {
      parent = newParent;
    }
  });

  /**
   * Add a listener to the current Jade.
   *
   * NOTE: Jade's only have 1 'topic' that they listen to, and that is changes in
   * value. So the @subscribe and @notify functions do not operate as other
   * publish/subscribe libraries might as you can not subscribe a listener to
   * particular events.
   *
   * @memberof Jade
   * @method
   * @name Jade#subscribe
   * @param {function} listener - A function you want called when the Jade's
   * value changes.
   */
  Object.defineProperty(me, 'subscribe', {
    value: function (listener) {
      listeners.push(listener);
    }
  });

  /**
   * Notify this Jade that it has changed. Will call all listeners that have been
   * added with the new value of this Jade.
   *
   * NOTE: Jade's only have 1 'topic' that they listen to, and that is changes in
   * value. So the @subscribe and @notify functions do not operate as other
   * publish/subscribe libraries might as you can not subscribe a listener to
   * particular events.
   *
   * @memberof Jade
   * @method
   * @name Jade#notify
   */
  Object.defineProperty(me, 'notify', {
    value: function () {
      listeners.forEach(function (listener) {
        listener(value);
      });
    }
  });

  /**
   * Create a Jade heirarchy from a simple object.
   *
   * @memberof Jade
   * @method
   * @name Jade#write
   * @param {object} obj - An object you want converted into a Jade heirarchy.
   */
  Object.defineProperty(me, 'write', {
    value: function (obj) {
      var keys = Object.keys(obj), me = this;
      for (var i = 0; i < keys.length; ++i) {
        if (!me.hasOwnProperty(keys[i])) me.addProp(keys[i]);
        if (isPropWritable(obj[keys[i]])) me[keys[i]].write(obj[keys[i]]);
        else me[keys[i]].value = obj[keys[i]];
      }
    }
  });

  /**
   * Create a simple object from a Jade heirarchy.
   *
   * @memberof Jade
   * @method
   * @name Jade#read
   * @returns {object} - An object representing the same heirarchy as this Jade.
   */
  Object.defineProperty(me, 'read', {
    value: function () {
      var obj = {},
        me = this,
        keys = Object.keys(me);
      for (var i = 0; i < keys.length; ++i) {
        if (typeof me[keys[i]] === 'string') return me[keys[i]];
        if (Object.keys(me[keys[i]]).length > 0) obj[keys[i]] = me[keys[i]].read();
        else obj[keys[i]] = me[keys[i]].value;
      }
      return obj;
    }
  });

  if (obj) this.write(obj);
}

export default Jade;
