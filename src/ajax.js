import jul from './jul.js';
import './extend.js';
import './exists.js';
import './param.js';

var defaultOpts = {
  async: true,
  handlers: {},
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  },
  method: 'GET',
  password: '',
  url: 'https://jsonplaceholder.typicode.com/posts/1',
  user: ''
};

var handleParamsObj = function (url, params) {
  var newquery = jul.param(params);
  return (url.indexOf('?') > -1) ? url + '&' + newquery.slice(1) : url + newquery;
}

/**
 * @typedef  defaultHeaders
 * @property 'Accept'='application/json'
 * @property 'Content-Type'='application/json'
 */

/**
 * @callback requestCallback
 * @param    {object}        err
 * @param    {object}        resp
 */

/**
 * A simple but flexible ajax call
 *
 * @param {object}          [opts]                            All the options to make the ajax call. All properties are optional.
 * @param {string}          opts.method='GET'                 The method of the call. E.g. 'GET','PUT','POST',etc.
 * @param {string}          opts.url                          The url you want to call.
 * @param {boolean}         opts.async=true                   Whether you want the call to be async.
 * @param {string}          opts.user                         The username for the endpoint security.
 * @param {object}          opts.params                       An object with key/value pairs representing the parameters you want to pass.
 * @param {string}          opts.password                     The password for the endpoint security.
 * @param {object}          opts.handlers                     An object defining the handlers for different events fired by the request.
 * @param {function}        opts.handlers.abort               [Docs]{@link https://developer.mozilla.org/en-US/docs/Web/Events/abort}
 * @param {function}        opts.handlers.error               [Docs]{@link https://developer.mozilla.org/en-US/docs/Web/Events/error}
 * @param {function}        opts.handlers.load                [Docs]{@link https://developer.mozilla.org/en-US/docs/Web/Events/load}
 * @param {function}        opts.handlers.loadend             [Docs]{@link https://developer.mozilla.org/en-US/docs/Web/Events/loadend}
 * @param {function}        opts.handlers.loadstart           [Docs]{@link https://developer.mozilla.org/en-US/docs/Web/Events/loadstart}
 * @param {function}        opts.handlers.progress            [Docs]{@link https://developer.mozilla.org/en-US/docs/Web/Events/progress}
 * @param {function}        opts.handlers.readystatechange=cb [Docs]{@link https://developer.mozilla.org/en-US/docs/Web/Events/readystatechange}
 * @param {function}        opts.handlers.timeout             [Docs]{@link https://developer.mozilla.org/en-US/docs/Web/Events/timeout}
 * @param {object}          opts.data                         The data you want to send for call. E.g. 'PUT' and 'POST' calls.
 * @param {defaultHeaders}  opts.headers                      An object defining the headers as key-value pairs of strings.
 * @param {requestCallback} [cb]                              The callback you want to use as a readystatechange handler. Standard node (err,resp) callback.
 If ommitted, a readystatechange handler is required on the opts objet.
 */
jul.ajax = function (opts, cb) {
  var req = new XMLHttpRequest(),
    keys, k;

  // Create default readystatechange handler if callback exists
  if (cb) defaultOpts.handlers.readystatechange = function () {
    // Only run if the request is done
    if (req.readyState === 4) {
      // If it's a successful request
      if (req.status >= 200 && req.status < 300) {
        var result = req.response;
        // See if we can parse to json or just return as text
        try {
          result = JSON.parse(req.responseText);
        } catch (e) {
          result = req.responseText;
        }
        // Call the callback function
        cb(null, result);
      } else {
        cb(req.response, null);
      }
    }
  };

  opts = jul.extend({}, defaultOpts, opts);

  // Convert param object into query string
  if (opts.params) opts.url = handleParamsObj(opts.url, opts.params);

  req.open(opts.method, opts.url, opts.async, opts.user, opts.password);

  keys = Object.keys(opts.handlers);
  for (k in keys) req['on' + keys[k]] = opts.handlers[keys[k]];

  keys = Object.keys(opts.headers);
  for (k in keys) {
    req.setRequestHeader(keys[k], opts.headers[keys[k]]);
  }

  req.send(opts.data);
};
