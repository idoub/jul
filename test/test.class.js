describe('Test Class', function () {
  describe('Test jul().addClass', function () {
    it('It works', function () {
      jul('#div1').addClass('testclass');
      chai.assert.isTrue(document.getElementById('div1').classList.contains('testclass'));
    });
    it('It works with multiple', function () {
      jul('#div2').addClass(['testclass1', 'testclass2']);
      chai.assert.isTrue(document.getElementById('div2').classList.contains('testclass1'));
      chai.assert.isTrue(document.getElementById('div2').classList.contains('testclass2'));
    });
  });

  describe('Test jul().removeClass', function () {
    it('It works', function () {
      jul('#div1').removeClass('testclass');
      chai.assert.isFalse(document.getElementById('div1').classList.contains('testclass'));
    });
    it('It works with multiple', function () {
      jul('#div2').removeClass(['testclass1', 'testclass2']);
      chai.assert.isFalse(document.getElementById('div2').classList.contains('testclass1'));
      chai.assert.isFalse(document.getElementById('div2').classList.contains('testclass2'));
    });
  });
});
