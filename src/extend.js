import jul from './jul.js';
import './is.js';

/**
 * Deep extend an already wrapped object.
 *
 * @param  {...*} o - Any objects you want to grab properties from and add to the currently wrapped object.
 * @return {object} - The wrapped object that has been extended with additional parameters.
 */
jul.prototype.extend = function (o) {
  var args = [].slice.call(arguments);
  for (var i = 0; i < this.e.length; ++i) {
    args.unshift(this.e[i]);
    jul.extend.apply(this, args);
  }
  return this;
};

/**
 * A deep extend method.
 *
 *
 * @param  {object}    out - The object that you want to extend.
 * @param  {...object} o   - Any objects you want to grab properties from and add to the extended object.
 * @return {object}        - The first parameter with added properties.
 */
jul.extend = function (out, o) {
  out = out || {};
  if (!jul.is('object', out)) return out;
  for (var i = 1; i < arguments.length; ++i) {
    var obj = arguments[i];
    if (!obj || !jul.is('object', obj)) continue;
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) {
        out[key] = jul.is('object', out[key]) ? this.extend(out[key], obj[key]) : obj[key];
      }
    }
  }
  return out;
};
