// jshint  esversion: 6
export default {
  input: './src/main.js',
  output: {
    file: './dist/jul.js',
    format: 'iife',
    name: 'jul',
    sourcemap: true
  },
};
