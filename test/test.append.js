describe('Test Append', function () {
  it('Appends to single element', function () {
    jul('#div1').append('Hello World!');
    chai.assert.equal(document.getElementById('div1').innerText, 'Hello World!');
  });
  it('Appends to multiple elements', function () {
    jul('.red').append('<span class="appended"></span>');
    chai.assert.isTrue(document.getElementsByClassName('appended').length === 2);
  });
  it('Appends to existing content', function () {
    jul('#div1').append('I\'m so happy');
    chai.assert.equal(document.getElementById('div1').innerHTML, 'Hello World!<span class="appended"></span>I\'m so happy');
  })
  it('Appends element', function () {
    var el = document.createElement('span');
    el.className = 'appended';
    jul('#div1').append(el);
    chai.assert.isTrue(document.getElementById('div1').children.length === 2);
    chai.assert.isTrue(document.getElementById('div1').childNodes.length === 4);
  })
});
