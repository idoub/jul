import jul from './jul.js';
import './each.js';
import './is.js';

/**
 * Provides an object representing the data attributes of the wrapped elements.
 *
 * @return {object} - An object mapping attribute names to their values.
 */
jul.prototype.data = function () {
  var out = {};
  this.each(function (el) {
    if (el.hasAttributes()) {
      var attrs = el.attributes;
      for (var i = 0; i < attrs.length; ++i) {
        var data = attrs[i].name.split('data-')[1];
        if (typeof data !== 'undefined') {
          if (out[data]) {
            if (!jul.is('array', out[data])) out[data] = [out[data]];
            out[data].push(attrs[i].value);
          } else {
            out[data] = attrs[i].value;
          }
        }
      }
    }
  });
  return out;
};
