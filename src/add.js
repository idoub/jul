import jul from './jul.js';

/**
 * Get a new jul from the concatenation of an existing jul and a new one constructed from the passed parameter.
 *
 * @param  {object|string} obj - The object you want wrapped and concatenated or a string representing a css selector.
 * @return {jul.prototype}     - A new jul wrapping both the original object and the added object.
 */
jul.prototype.add = function (obj) {
  return jul(this.e.concat(jul(obj).e));
};
