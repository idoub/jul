import jul from './jul.js';
import './each.js';
import './is.js';

/**
 * Adds a class or classes to wrapped elements
 *
 * @param  {string|string[]} cls - A class or array of classes that you want to add to the wrapped elements.
 * @return {jul.prototype}       - The jul that the method was called on.
 */
jul.prototype.addClass = function (cls) {
  if (!jul.is('array', cls)) cls = [cls];
  this.each(function (el) {
    var classList = el.className.split(' ');
    for (var j = 0; j < cls.length; j++) {
      if (classList.indexOf(cls[j]) === -1) classList.push(cls[j]);
    }
    el.className = classList.join(' ');
  });
  return this;
};
