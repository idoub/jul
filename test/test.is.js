describe('Test Is', function () {
  it('Is array', function () {
    chai.assert.isTrue(jul.is('array', [1, 2, 3]));
    chai.assert.isFalse(jul.is('array', true));
    chai.assert.isFalse(jul.is('array', null));
  });
  it('Is boolean', function () {
    chai.assert.isTrue(jul.is('boolean', true));
    chai.assert.isTrue(jul.is('boolean', false));
    chai.assert.isFalse(jul.is('boolean', [1, 2, 3]));
    chai.assert.isFalse(jul.is('boolean', null));
  });
  it('Is date', function () {
    chai.assert.isTrue(jul.is('date', new Date()));
    chai.assert.isFalse(jul.is('date', true));
    chai.assert.isFalse(jul.is('date', null));
  });
  it('Is error', function () {
    chai.assert.isTrue(jul.is('error', new Error()));
    chai.assert.isFalse(jul.is('error', true));
    chai.assert.isFalse(jul.is('error', null));
  });
  it('Is function', function () {
    chai.assert.isTrue(jul.is('function', function () { return true; }));
    chai.assert.isFalse(jul.is('function', true));
    chai.assert.isFalse(jul.is('function', null));
  });
  it('Is null', function () {
    chai.assert.isTrue(jul.is('null', null));
    chai.assert.isFalse(jul.is('null', undefined));
  });
  it('Is number', function () {
    chai.assert.isTrue(jul.is('number', 1));
    chai.assert.isFalse(jul.is('number', '1'));
  });
  it('Is object', function () {
    chai.assert.isTrue(jul.is('object', { foo: 'bar' }));
    chai.assert.isFalse(jul.is('object', [1, 2, 3]));
    chai.assert.isFalse(jul.is('object', '1, 2, 3'));
    chai.assert.isFalse(jul.is('object', true));
    chai.assert.isFalse(jul.is('object', null));
  });
  it('Is regexp', function () {
    chai.assert.isTrue(jul.is('regexp', /\s/g));
    chai.assert.isTrue(jul.is('regexp', new RegExp('.*')));
    chai.assert.isFalse(jul.is('regexp', '.*'));
  });
  it('Is string', function () {
    chai.assert.isTrue(jul.is('string', '1, 2, 3'));
    chai.assert.isFalse(jul.is('string', true));
    chai.assert.isFalse(jul.is('string', null));
    chai.assert.isFalse(jul.is('string', undefined));
  });
  it('Is undefined', function () {
    chai.assert.isTrue(jul.is('undefined', undefined));
    chai.assert.isTrue(jul.is('undefined', chai.frubths));
    chai.assert.isFalse(jul.is('undefined', null));
    chai.assert.isFalse(jul.is('undefined', {}));
  });
});
