import jul from './jul.js';

/**
 * Loop through every member of an arraylike object wrapped by a jul and map them to a different array.
 *
 * @param  {function} cb - The mapping function. The value returned from the function will be in the new array.
 * @param  {...*} params - Any params you want passed to the mapping function.
 * @return {array}       - The new array with values mapped from the current wrapped arraylike object.
 *
 * @example
 *  jul(['jul','jade','ruby','jasper','diamond','turquoise']).map(
 *      function(element,index,array,prefix){
 *          return prefix+element.charAt(0).toUpperCase()+element.slice(1);
 *      },
 *      'my '
 *  );
 */
jul.prototype.map = function (cb, params) {
  var args = [].slice.call(arguments), results = [], me = this;
  args.shift();
  for (var key in Object.keys(me.e)) {
    results.push(cb.apply(me, [me.e[key], key, me.e].concat(args)));
  }
  return results;
};
