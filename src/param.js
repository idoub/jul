import jul from './jul.js';
import './is.js';

var convertObjToQuery = function (obj) {
  var paramArray = [];
  for (var key in obj) {
    if (obj.hasOwnProperty(key)) {
      var val = obj[key];
      if (jul.is('array', val)) {
        val = val[0];
        for (var i = 1; i < obj[key].length; ++i) {
          val += '&' + key + '=' + obj[key][i]
        }
      }
      paramArray.push(key + '=' + val);
    }
  }
  return '?' + paramArray.join('&');;
}

var convertQueryToObj = function (str) {
  if (str.indexOf('?') > -1) str = str.slice(str.indexOf('?') + 1);
  var params = {};
  var paramArray = str.split('&');
  for (var i = 0; i < paramArray.length; ++i) {
    var keyval = paramArray[i].split('=');
    if (params[keyval[0]]) {
      if (!jul.is('array', params[keyval[0]])) params[keyval[0]] = [params[keyval[0]]];
      params[keyval[0]].push(keyval[1]);
    } else {
      params[keyval[0]] = keyval[1];
    }
  }
  return params;
}

/**
 * Given an object, converts the object to a query string parameter.
 * Given a query string, converts it to an object.
 *
 * @param  {object|string}   val - The variable to convert
 * @return {string|object} - True if the variable is a strict objct, otherwise false.
 */
jul.param = function (val) {
  if (jul.is('object', val)) return convertObjToQuery(val);
  else return convertQueryToObj(val);
}
