describe('Test Param', function () {
  describe('Test string to object', function () {
    it('Query to object', function () {
      var url = "foo=bar&hello=world";
      var obj = { foo: 'bar', hello: 'world' };
      chai.assert.deepEqual(jul.param(url), obj);
    });
    it('URL to object', function () {
      var url = "https://jsonplaceholder.typicode.com/posts?foo=bar&hello=world";
      var obj = { foo: 'bar', hello: 'world' };
      chai.assert.deepEqual(jul.param(url), obj);
    });
    it('URL to object with array', function () {
      var url = "https://jsonplaceholder.typicode.com/posts?foo=bar&foo=world";
      var obj = { foo: ['bar', 'world'] };
      chai.assert.deepEqual(jul.param(url), obj);
    });
  });
  describe('Test object to string', function () {
    it('Object to query', function () {
      var obj = { foo: 'bar', hello: 'world' };
      var url = "?foo=bar&hello=world";
      chai.assert.equal(jul.param(obj), url);
    });
    it('Object with array to query', function () {
      var obj = { foo: ['bar', 'world'] };
      var url = "?foo=bar&foo=world";
      chai.assert.equal(jul.param(obj), url);
    });
  });
});
