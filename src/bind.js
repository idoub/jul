import jul from './jul.js';
import Jade from './Jade.js';
import './each.js';
import './on.js';
import './ready.js';
import './find.js';
import './addClass.js';

var model = new Jade(),
  events = {};

var getDescendant = function (context, ancestry) {
  return ancestry.split('.').reduce(function (parent, child, i, arr) {
    return parent && parent[child] || (parent.addProp(child), parent[child]);
  }, context);
};

var getJade = function (element, jade, context) {
  context = context || model;
  if (!jade) jade = element.getAttribute('data-j') || '';
  return typeof jade === 'string' ? getDescendant(context, jade) : jade;
};

var getElementType = function (element) {
  var type = element.nodeName;
  if (type === 'INPUT') type = element.type.toUpperCase();
  return type;
};

var bindLoop = function (element, context) {
  if (element.children.length > 1) return console.log('Cannot bind multiple children within loop.');

  var path = element.getAttribute('data-j-loop'),
    jade = getJade(element, path, context),
    inner = element.inner || document.createDocumentFragment();

  if (inner.children.length === 0) inner.appendChild(element.children[0].cloneNode(true));
  element.innerHTML = '';

  jade.subscribe(function () {
    element.innerHTML = '';
    for (var i = 0; i < jade.value.length; ++i) {
      element.appendChild(inner.cloneNode(true));
      jul.bindAll(element.lastElementChild, new Jade(jade.value[i]));
    }
  });
  element.setAttribute('bound', true);
};

var bindData = function (element, context) {
  var binding = element.getAttribute('data-j').split(':'),
    path = binding[0],
    prop = binding[1],
    jade = getJade(element, path, context);

  element[prop] = jade.value;

  jade.subscribe(function () {
    if (prop === 'class') jul(element).addClass(jade.value);
    else element[prop] = jade.value;
  });

  jul(element).on('change input', function () {
    jade.value = element.value;
  });

  element.setAttribute('bound', true);
};

var bindEvent = function (element, context) {
  var binding = element.getAttribute('data-j-event').split(':'),
    evt = binding[0],
    name = binding[1],
    path = binding.length > 2 ? binding[2] : undefined,
    jade = path ? getJade(element, path, context) : undefined,
    cb = console.log;

  jul(element).on(evt, function (e) {
    e.preventDefault();
    if (events.hasOwnProperty(name)) cb = events[name];
    cb(e, jade.value);
  });
};

var bindElement = function (element, context) {
  if (element.getAttribute('data-j-event') && !element.getAttribute('bound')) bindEvent(element, context);
  if (element.getAttribute('data-j-loop') && !element.getAttribute('bound')) bindLoop(element, obj);
  if (element.getAttribute('data-j') && !element.getAttribute('bound')) bindData(element, context);
};

/**
 * Bind all html elements within an element that have a data-j* attribute.
 *
 * @param {Node}   [parent=document] - The parent within which you want to bind elements. Defaults to document.
 * @param {object} [context]         - The object you want to use to bind to the element.
 */
jul.bindAll = function (parent, context) {
  parent = parent || document;
  jul(parent).find('[data-j-event]:not([bound])').each(function (e) {
    bindEvent(e, context);
  });
  jul(parent).find('[data-j-loop]:not([bound])').each(function (e) {
    bindLoop(e, context);
  });
  jul(parent).find('[data-j]:not([bound])').each(function (e) {
    bindData(e, context);
  });
};

/**
 * Write a javascript object to a model. Model defaults to jul's internal model.
 *
 * @param {object} obj   - The object you want written to a model.
 * @param {Jade}   [mod] - The model you want the object to be written to.
 */
jul.writeModel = function (obj, mod) {
  (mod || model).write(obj);
};

/**
 * Read a model as a standard javascript object. Model defaults to jul's internal model.
 *
 * @param {Jade} [mod] - The model you want to read.
 */
jul.readModel = function (mod) {
  return (mod || model).read();
};

/**
 * Bind a function as an event on the page.
 *
 * If there is an element on the page with a binding that has the same name as
 * the name you are giving to the function, then the provided function will be
 * called when the event is triggered.
 *
 * @param {string}   name - The name of the function. If it is the same as a bound event name on the page, then the next parameter will be called when the event fires.
 * @param {function} cb   - The function you wish to use when a bound event with the same name fires.
 */
jul.bindEvent = function (name, cb) {
  events[name] = cb;
};

jul.ready(function (evt) {
  jul.bindAll(document);
});
