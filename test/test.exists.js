describe('Test Exists', function () {
  var obj1, obj2;
  beforeEach(function () {
    obj1 = {
      prop1: true,
      prop2: {
        deepProp: 'Foo Bar'
      }
    }
  });
  it('Exists works for direct properties', function () {
    chai.assert.isTrue(jul.exists(obj1, 'prop1'));
  });
  it('Exists works for prototype', function () {
    chai.assert.isTrue(jul.exists(function () { }, 'prototype'));
  });
  it('Exists doesn\'t work for non-existent properties', function () {
    chai.assert.isFalse(jul.exists(obj1, 'prop3'));
  });
  it('Exists doesn\'t work for __proto__', function () {
    chai.assert.isFalse(jul.exists(obj1, '__proto__'));
  });
});
