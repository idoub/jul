import jul from './jul.js';

/**
 * Checks whether a variable is a certain type.
 *
 * @param  {*}       val  - The variable you want to check
 * @param  {string}  type - The type you want to compare the variable to.
 * @return {boolean}      - True if the variable is that type, otherwise false.
 */
jul.is = function (type, val) {
  switch (type) {
    case 'array': return Array.isArray(val);
    case 'boolean': return typeof val === 'boolean';
    case 'date': return val instanceof Date;
    case 'error': return val instanceof Error && typeof val.message !== 'undefined';
    case 'function': return typeof val === 'function';
    case 'null': return val === null;
    case 'number': return typeof val === 'number' && isFinite(val);
    case 'object': if (val == null) return false; return val && typeof val === 'object' && val.constructor === Object;
    case 'regexp': return val && typeof val === 'object' && val.constructor === RegExp;
    case 'string': return typeof val === 'string' || val instanceof String;
    case 'undefined': return typeof val === 'undefined';
    default: return false;
  }
}
