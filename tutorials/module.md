# Table of Contents
- [Introduction](#introduction)
- [Example](#example)
- [Static vs Function Methods](#static-vs-function-methods)
- [Private Methods and Properties](#private-methods-and-properties)
- [Add an Existing Library](#add-an-existing-library)

# Introduction
**JUL** uses [rollup.js](https://rollupjs.org) for packaging, so modules are defined using the standard [ES module syntax](https://rollupjs.org/#es-module-syntax).

# Example
```javascript
// Define dependencies. All modules depend on the base JUL module.
import _ from './jul.js';
import extend from './extend.js';

// This is a global variable and will be accessible everywhere in JUL
var default = {'foo': bar};

// This method is defined directly on the jul object. It will be accessible by jul.staticMethod.
_.staticMethod = function(params){
  // Using a required module.
  jul.extend(default,params)
};

// This method is defined on the jul function. It will be accessible by jul().functionMethod.
_.prototype.functionMethod = function(params){};

// Re-export JUL with your new definitions on it.
export default _;

// Now when using the compiled library, we can access two new methods.
jul.staticMethod({hello:'world'});
jul('.style').functionMethod('something');
```

# Static vs Function Methods
Notice the difference in how to declare static methods vs function methods in the example above. We won't go into the prototype inheritance of **JUL** here (see {@tutorial intro} to learn more), we just need to remember that in the module constructor function, any methods added using `_.method = function(){}` will be accessible by calling `jul.method()`, and any methods added using `_.prototype.method = function(){}` will be accessible by calling `jul().method()`. In the first case, the `this` keyword references the base `jul` constructor. In the second case, `this` references a `jul` object instance.

# Private Methods and Properties
Since we are just using ES6 modules, if we want private methods and/or properties, we'll have to wrap the module in it's own IIFE.

For example:
```javascript
_.foo = (function(){
  var privateProperty = 'Hello World';

  var privateMethod = function() {
    console.log(privateProperty);
  }

  return function(prop) {
    if(prop) privateProperty = prop;
    privateMethod();
  };
}());
```
will define a new module, **foo**, that can be called using `jul.foo()`. Neither `privateProperty` or `privateMethod` are available to the outside, but calling `jul.foo('bar')` will update `privateProperty` and then call `privateMethod`, so the console will log `"bar"`.

***However*** this is strongly advised against, as it creates hidden, internal state for your modules. Observe the following:
```javascript
jul.foo();      // prints "Hello World"
jul.foo('bar'); // prints "bar"
jul.foo();      // prints "bar"
```
If another module unexpectedly calls one of the functions that updates internal state, then a user of your module, expecting a specific state, may end up with a very difficult bug to track down.

There are some situations though where this can be useful. Say for example, a pseudo-random number generator that maintains and updates an internal **seed** to create a reproducible sequence of numbers given a specific seed.

The conclusion? Internal state can be both useful and dangerous, so use it wisely.

# Add an Existing Library
The module system makes it fairly straightforward to add an existing library to **JUL**. Simply define it in it's own file, add it as a property on either `jul` or `jul.prototype` and export from the file.

For example, if you have a library function **square**:
```javascript
var square = function(val) {
  return val*val;
}
```
you can create a new module from the function like so:
```javascript
import _ from './jul.js';
_.square = function(val) {
  return val*val;
}
export default _;
```
or by wrapping in an IIFE:
```javascript
import _ from './jul.js';
_square = (function(){
  var square = function(val){
    return val*val;
  }
  return square;
}());
export default _;
```
You will notice that the **square** code is completely unmodified.