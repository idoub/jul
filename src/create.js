import jul from './jul.js';

jul.create = function (str, isSVG) {
  var tokens = str.split(/([>\+\^\(\)\*#\.\[\]\{\}])/g).filter(function (s) { return s.length; }),
    attributeRegex = /(\S*=".*?")|(\S*=\S*)|(\S+)/gi,
    parent = { type: '_root', children: [] },
    newNode = null,
    c = 0,
    consume = function () { return tokens[c++]; },
    peek = function () { return tokens[c]; },
    nodeMap = {
      'bq': 'blockquote'
    },
    implicitMap = {
      'em': 'span',
      'ul': 'li',
      'table': 'tr',
      'tr': 'td',
      'p': 'span'
    };

  var mapImplicit = function (type) {
    if (!type) return implicitMap[parent.type] ? implicitMap[parent.type] : 'div';
    return nodeMap[type] ? nodeMap[type] : type || 'div';
  };

  var makeNode = function (type) {
    if (newNode) return newNode;
    newNode = {
      attributes: '',
      children: [],
      classes: [],
      id: '',
      parent: parent,
      text: '',
      type: mapImplicit(type)
    }
    parent.children.push(newNode);
    return newNode;
  };

  var ops = {
    '>': function () {
      parent = newNode;
      newNode = null;
    },
    '+': function () {
      newNode = null;
    },
    '^': function () {
      parent = parent.parent;
      newNode = null;
    },
    '(': function () {
      parent = makeNode('_group');
      newNode = null;
    },
    ')': function () {
      do { newNode = newNode.parent } while (newNode.type !== '_group');
      parent = newNode.parent;
    },
    '*': function () {
      newNode.count = parseInt(consume());
    },
    '#': function () {
      makeNode();
      newNode.id = consume();
    },
    '.': function () {
      makeNode();
      newNode.classes.push(consume());
    },
    '[': function () {
      makeNode();
      var attrs = '';
      while (peek() !== ']') attrs += consume();
      newNode.attributes = attrs;
    },
    ']': function () { /* noop */ },
    '{': function () {
      makeNode();
      newNode.text = consume();
    },
    '}': function () { /* noop */ }
  };

  var parseExpr = function () {
    ops[peek()] ? ops[consume()]() : makeNode(consume());
    while (peek()) parseExpr();
    while (parent.parent) parent = parent.parent;
    return parent;
  };

  var tree = parseExpr();

  var handleNumbering = function (val, inc, max) {
    if (val.indexOf('$') === -1) return val;
    var base = '1';
    var keyval = val.match(/^([^@]*)@?(.*)$/);
    val = keyval[1];
    base = keyval[2] || '1';
    if (base === '-') base += '1';
    base = parseInt(base);
    inc = (base < 0) ? max - inc - base - 1 : inc + base;
    return val.replace(/(\$+)/, function (d) {
      return ('0000' + inc).slice(-d.length);
    })
  };

  var handleAttributes = function (child, childElement, inc, max) {
    child.attributes.match(attributeRegex).forEach(function (attr) {
      if (attr.indexOf('=') !== -1) {
        var keyVal = attr.split('=');
        var val = keyVal[1];
        if (val[0] === '"' || val[0] === '\'') val = val.slice(1, -1);
        childElement.setAttribute(keyVal[0], handleNumbering(val, inc, max));
      } else {
        childElement.setAttribute(attr, '');
      }
    });
  };

  var createElement = function (type) {
    if (isSVG) return document.createElementNS('http://www.w3.org/2000/svg', type);
    if (type === '_group') return document.createDocumentFragment();
    return document.createElement(type);
  };

  var makeElement = function (child, inc) {
    var childElement = createElement(handleNumbering(child.type, inc, child.count));
    if (child.id) childElement.id = handleNumbering(child.id, inc, child.count);
    if (child.classes.length > 0) childElement.className = handleNumbering(child.classes.join(' '), inc, child.count);
    if (child.attributes) handleAttributes(child, childElement, inc, child.count);
    if (child.text) childElement.innerText = handleNumbering(child.text, inc, child.count);
    if (child.children.length > 0) childElement.appendChild(generate(child, inc));
    return childElement;
  };

  var generate = function (branch, inc) {
    var wrapper = document.createDocumentFragment();
    for (var i = 0; i < branch.children.length; ++i) {
      var child = branch.children[i];
      if (child.count) {
        for (var j = 0; j < child.count; j++) {
          wrapper.appendChild(makeElement(child, j));
        }
      } else {
        wrapper.appendChild(makeElement(child, inc));
      }
    }
    return wrapper;
  };

  return generate(tree, 0);
}
