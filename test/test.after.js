describe('Test After', function () {
  it('It works for a single element', function () {
    jul('#div2').after('<div class="afterelement"></div>');
    chai.assert.isTrue(document.getElementsByClassName('afterelement').length === 1);
  });
  it('It works with multiple elements', function () {
    jul('.red').after('<div class="afterelement"></div>');
    chai.assert.isTrue(document.getElementsByClassName('afterelement').length === 3);
  });
  it('It works with a node', function () {
    var el = document.createElement('div');
    el.id = 'insertednode';
    jul('#div1').after(el);
    chai.assert.isOk(document.getElementById('insertednode'));
  });
});
