import jul from './jul.js';
import './each.js';

/**
 * Get a new jul from a query of elements within a current jul.
 *
 * @param  {string}        query - The query string you want to use to search within the current jul.
 * @return {jul.prototype}       - A new jul if found, otherwise the current jul.
 */
jul.prototype.find = function (query) {
  if (typeof query !== 'string') return this;
  var arr = [];
  this.each(function (e) {
    if (e instanceof Node) arr = arr.concat([].slice.call(e.querySelectorAll(query)));
  });
  return jul(arr);
};
