import jul from './jul.js';

/**
 * Loop through every member wrapped by a jul and run a function on each.
 *
 * @param  {function} cb   - The callback you want run on each member.
 * @param  {...*} params   - Any params you want passed to the callback.
 * @return {jul.prototype} - The jul that the method was called on.
 *
 * @example
 *  jul('.hidden').each(function(element,index,array,newClass,color){
 *      element.classList.add(newClass);
 *      element.style.color = color;
 *  },'shown','#444444');
 */
jul.prototype.each = function (cb, params) {
  var args = [].slice.call(arguments), me = this;
  args.shift();
  for (var key in Object.keys(me.e)) {
    cb.apply(me, [me.e[key], key, me.e].concat(args));
  }
  return me;
};

/**
 * Loop through every member of an array passed as an object and run a function on each element.
 *
 * @param  {function} cb - The callback you want run on each member.
 * @param  {...*} params - Any params you want passed to the callback.
 * @return {array}       - The original array, modified by the callback function.
 *
 * @example
 *  jul.each([2,4,8,16],function(num,i,array,exponent){
 *      array[i] = Math.pow(num,exponent);
 *  },3);
 */
jul.each = function (arr, cb, params) {
  return jul(arr).each(cb, params).e;
};
