import jul from './jul.js';
import './each.js';
import './is.js';

/**
 * Append the provided content to the end of each of the currently wrapped elements.
 *
 * @param  {node|string}   content - The content you want appended to each of the currently wrapped elements.
 * @return {jul.prototype}         - The jul that the method was called on.
 */
jul.prototype.append = function (content) {
  var frag = document.createDocumentFragment();
  if (jul.is('string', content)) {
    var div = document.createElement('div');
    div.innerHTML = content;
    for (var i = 0; i < div.childNodes.length; ++i) {
      frag.appendChild(div.childNodes[i]);
    }
  } else {
    frag.appendChild(content);
  }
  this.each(function (el) {
    if (el instanceof HTMLElement) el.appendChild(frag.cloneNode(true));
  });
  return this;
};
